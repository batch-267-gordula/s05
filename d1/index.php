<?php
    //$_GET and $_POST are "super global" variables in PHP.
        // Allow you to retrieve information sent by the client.

    // "super global" variable allows data to persists between pages or a single session.

    // Both $_GET and $_POST create an associative array that holds key=>value pair.
        // "key" represents the name of the form control element.
        // "value" represents the inputted data from the user.

    // commonly output an associative array
    // var_dump($_GET);
    // var_dump($_POST);

    $tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

    // isset() function checsk wether a variable is set or check if already existed

        // true if a variable/key is set, return false if the return is NULL.
    if(isset($_GET["index"])) {
        //$_GET["index"]; -- gets the value in the form control which is stored in the $indexGet variable
    $indexGet = $_GET["index"];
    echo "The retrieved taks from GET is $tasks[$indexGet]. <br>";
    }

    if(isset($_POST["index"])) {
        //$_GET["index"]; -- gets the value in the form control which is stored in the $indexGet variable
    $indexPost = $_POST["index"];
    echo "The retrieved taks from POST is $tasks[$indexPost]. <br>";
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05: Client-Server Communication ($_GET and $_POST)</title>
</head>
<body>
    <h1>Task index from GET</h1>
    <form method="GET">
        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        <button type="submit">GET</button>

        <!-- <label>Email:</label>
        <input type="email" name="email">
        <br>
        <label>Password:</label>
        <input type="password" name="password">
        <br>
        <button type="submit">Login</button> -->
    </form>
    
    <h1>Task index from POST</h1>
    <form method="POST">
        <select name="index" required>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        <button type="submit">POST</button>
    </form>
</body>
</html>