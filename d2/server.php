<?php session_start(); 
/* session_start() is also added in the server.php so it is also included in the current session. */

// session variable
// this will holds all the inputted value from the user once they clicked the 'add' button
var_dump($_SESSION['tasks']);

// Create a TaskList class to hold the methods in adding, viewing, editing, and deleting task.

// add task
class TaskList{
    public function add($description){
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];

        // Session variables are set with PHP global variables : $_SESSION

        if($_SESSION['tasks'] === null){
            $_SESSION['tasks'] = array();
        }

        // The $newTask will be added in the $_SESSION['tasks']variable

        array_push($_SESSION['tasks'], $newTask);
    }

    // update a task
    public function update($id, $updatedDescription, $updatedIsFinished){

        $_SESSION['tasks'][$id] -> description = $updatedDescription;
        $_SESSION['tasks'][$id] -> isFinished = ($updatedIsFinished !== null) ? true : false;
    }

    // deleta a task
    public function delete($id){
        // Syntax: array_splice(array, startDel, length, newArrElement)
        // A task will be deleted from the "$_SESSION['tasks']" base on its $id(index) and will only removed one task

        array_splice($_SESSION['tasks'], $id, 1);
    }

    // clear all tasks method
    public function clear(){
        
        // removes all the data associated with the current sesssion
        session_destroy();
    }
}

// Instantiation of TaskList
// tasklist is instantiated from the TaskList() class to have access with its method
$taskList = new TaskList();

// Create an if statement to handle the client request and identify the action needed to execute.
    // This sif statement will handle the action by the user.
    // taskLists method will invoke on the action

// Adding task
if($_POST['action'] === 'add') {
    $taskList->add($_POST['description']);
}
// Updating task
else if($_POST['action'] === 'update'){
    $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}
// Deleting task
else if($_POST['action'] === 'delete') {
    $taskList->delete($_POST['id']);
}
// Clearing all the tasks in our task lists
else if($_POST['action'] === 'clear') {
    $taskList->clear();
}










//header()
// This is used to send raw HTTP header, but his can also be used to redirect us on specific location
// it will redirect us to the index.php file upon sending the request
header("location: ./index.php");

?>